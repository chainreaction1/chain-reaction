import React from "react";

const P404 = () => {
  return (
    <div style={{ fontSize: "3em", color: "white" }}>404 Page Not Found</div>
  );
};

export default P404;
